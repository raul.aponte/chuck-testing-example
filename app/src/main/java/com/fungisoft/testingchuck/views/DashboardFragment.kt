package com.fungisoft.testingchuck.views

import android.app.Fragment
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.fungisoft.testingchuck.R
import com.fungisoft.testingchuck.models.Joke
import com.fungisoft.testingchuck.network.JokesListener
import com.fungisoft.testingchuck.views.adapters.JokesAdapter
import com.fungisoft.testingchuck.views.navigation.NavigationListener

class DashboardFragment : Fragment() {
    companion object {
        fun newInstance(listener: NavigationListener): DashboardFragment {
            val fragment = DashboardFragment()
            fragment.navigationListener = listener
            return fragment
        }
    }

    private var navigationListener: NavigationListener? = null

    private var searchInputLayout: TextInputLayout? = null
    private var searchEditText: EditText? = null
        get() = searchInputLayout?.editText
    private var searchButton: Button? = null
    private var recyclerView: RecyclerView? = null
    private var categoriesButton: Button? = null
    private var randomButton: Button? = null

    private var jokesAdapter: JokesAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        jokesAdapter = JokesAdapter {
            fragmentManager.beginTransaction()
                    .replace(R.id.layoutMainContent, JokeContentFragment.newInstance(it))
                    .addToBackStack("")
                    .commit()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater?.inflate(R.layout.fragment_dashboard, container, false)
                ?: return super.onCreateView(inflater, container, savedInstanceState)

        linkViews(view)
        linkEvents()
        initView()

        return view
    }

    private fun initView() {
        recyclerView?.adapter = jokesAdapter
    }

    private fun linkViews(view: View) {
        searchInputLayout = view.findViewById(R.id.searchTextInputLayout)
        searchButton = view.findViewById(R.id.searchButton)
        recyclerView = view.findViewById(R.id.recyclerView)
        categoriesButton = view.findViewById(R.id.categoriesButton)
        randomButton = view.findViewById(R.id.randomButton)
    }

    private fun linkEvents() {
        categoriesButton?.setOnClickListener {
            navigationListener?.showCategories()
        }
        randomButton?.setOnClickListener {
            navigationListener?.showRandomJoke()
        }
        searchButton?.setOnClickListener {
            val text = searchEditText?.text.toString()
            navigationListener?.searchForJoke(text, object : JokesListener {
                override fun callback(jokes: List<Joke>) {
                    showJokes(jokes)
                }

            })
        }
    }

    private fun showJokes(jokes: List<Joke>) {
        jokesAdapter?.jokes = jokes
    }
}
