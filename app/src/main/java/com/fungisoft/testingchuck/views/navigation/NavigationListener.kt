package com.fungisoft.testingchuck.views.navigation

import com.fungisoft.testingchuck.network.JokesListener

interface NavigationListener {
    fun onCategorySelected(category: String)
    fun searchForJoke(text: String, callback: JokesListener)
    fun showCategories()
    fun showRandomJoke()
}