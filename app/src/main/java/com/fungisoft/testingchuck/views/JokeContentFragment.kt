package com.fungisoft.testingchuck.views

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.fungisoft.testingchuck.R
import com.fungisoft.testingchuck.models.Joke
import com.squareup.picasso.Picasso

class JokeContentFragment : Fragment() {
    companion object {
        private const val ARG_JOKE = "JOKE"

        fun newInstance(joke: Joke): JokeContentFragment {
            val fragment = JokeContentFragment()
            val args = Bundle()
            args.putParcelable(ARG_JOKE, joke)
            fragment.arguments = args
            return fragment
        }
    }

    private var imageView: ImageView? = null
    private var contentTextView: TextView? = null

    private var joke: Joke? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        joke = arguments.getParcelable(ARG_JOKE)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater?.inflate(R.layout.fragment_joke_content, container, false)
                ?: return super.onCreateView(inflater, container, savedInstanceState)

        linkViews(view)
        initView()

        return view
    }

    private fun initView() {
        joke?.let { showJoke(it) }
    }

    private fun showJoke(joke: Joke) {
        contentTextView?.text = joke.value
        Picasso.get().load(joke.iconUrl).into(imageView)
    }

    private fun linkViews(view: View) {
        imageView = view.findViewById(R.id.imageView)
        contentTextView = view.findViewById(R.id.contentTextView)
    }
}
