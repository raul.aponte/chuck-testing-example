package com.fungisoft.testingchuck.views.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.fungisoft.testingchuck.R

class CategoriesAdapter(private val onItemClickListener: (String) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var categories: List<String> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_category, parent, false)
        return ItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return categories.count()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ItemViewHolder
        viewHolder.category = categories[position]
        viewHolder.onItemClickListener = onItemClickListener
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val categoryTextView: TextView = view.findViewById(R.id.categoryTextView)
        var category: String
            get() = categoryTextView.text.toString()
            set(value) {
                categoryTextView.text = value
            }
        var onItemClickListener: ((String) -> Unit)? = null
            set(value) {
                field = value
                itemView.setOnClickListener {
                    category.let { it1 -> onItemClickListener?.invoke(it1) }
                }
            }
    }
}
