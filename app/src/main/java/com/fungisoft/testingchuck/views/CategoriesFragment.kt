package com.fungisoft.testingchuck.views

import android.app.Fragment
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fungisoft.testingchuck.R
import com.fungisoft.testingchuck.views.adapters.CategoriesAdapter
import com.fungisoft.testingchuck.views.navigation.NavigationListener

class CategoriesFragment: Fragment() {
    companion object {
        private const val ARGS_CATEGORIES = "categories"

        fun newInstance(categories: List<String>, listener: NavigationListener?): CategoriesFragment {
            val fragment = CategoriesFragment()
            fragment.navigationListener = listener
            val args = Bundle()
            args.putStringArray(ARGS_CATEGORIES, categories.toTypedArray())
            fragment.arguments = args
            return fragment
        }
    }

    private var navigationListener: NavigationListener? = null

    private var categoriesAdapter: CategoriesAdapter? = null

    private var recyclerView: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        categoriesAdapter = CategoriesAdapter {
            navigationListener?.onCategorySelected(it)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater?.inflate(R.layout.fragment_categories, container, false)
                ?: return super.onCreateView(inflater, container, savedInstanceState)

        linkViews(view)
        initView()

        return view
    }

    private fun linkViews(view: View) {
        recyclerView = view.findViewById(R.id.recyclerView)
    }

    private fun initView() {
        val categories = arguments?.getStringArray(ARGS_CATEGORIES)
        if (categories != null) {
            categoriesAdapter?.categories = categories.toList()
        }
        recyclerView?.adapter = categoriesAdapter
    }
}
