package com.fungisoft.testingchuck.views.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.fungisoft.testingchuck.R
import com.fungisoft.testingchuck.models.Joke
import com.squareup.picasso.Picasso

class JokesAdapter(private val onItemClickListener: (Joke) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var jokes: List<Joke> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_joke, parent, false)
        return ItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return jokes.count()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ItemViewHolder
        viewHolder.joke = jokes[position]
        viewHolder.onItemClickListener = onItemClickListener
    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val jokeTextView: TextView = view.findViewById(R.id.jokeTextView)
        val imageView: ImageView = view.findViewById(R.id.imageView)
        var joke: Joke? = null
            set(value) {
                jokeTextView.text = joke?.value
                Picasso.get().load(joke?.iconUrl).into(imageView)
            }
        var onItemClickListener: ((Joke) -> Unit)? = null
            set(value) {
                field = value
                itemView.setOnClickListener {
                    joke?.let { it1 -> onItemClickListener?.invoke(it1) }
                }
            }
    }
}
