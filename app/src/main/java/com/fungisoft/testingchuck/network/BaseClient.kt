package com.fungisoft.testingchuck.network

import com.fungisoft.testingchuck.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

open class BaseClient {
    val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
}
