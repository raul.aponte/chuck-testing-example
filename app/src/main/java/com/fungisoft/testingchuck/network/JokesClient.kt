package com.fungisoft.testingchuck.network

import com.fungisoft.testingchuck.models.Joke
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

class JokesClient : BaseClient(), JokesProvider {
    private val service = retrofit.create(JokesService::class.java)

    override fun joke(callback: JokeListener) {
        service.joke().enqueue(object : Callback<Joke> {
            override fun onResponse(call: Call<Joke>?, response: Response<Joke>?) {
                response?.body()?.let {
                    callback.callback(it)
                }
            }
            override fun onFailure(call: Call<Joke>?, t: Throwable?) {

            }
        })
    }

    override fun joke(id: String, callback: JokeListener) {
        service.joke(id).enqueue(object : Callback<Joke> {
            override fun onResponse(call: Call<Joke>?, response: Response<Joke>?) {
                response?.body()?.let {
                    callback.callback(it)
                }
            }
            override fun onFailure(call: Call<Joke>?, t: Throwable?) {

            }
        })
    }

    override fun fromCategory(category: String, callback: JokeListener) {
        service.fromCategory(category).enqueue(object : Callback<Joke> {
            override fun onResponse(call: Call<Joke>?, response: Response<Joke>?) {
                response?.body()?.let {
                    callback.callback(it)
                }
            }
            override fun onFailure(call: Call<Joke>?, t: Throwable?) {

            }
        })
    }

    override fun search(text: String, callback: JokesListener) {
        service.search(text).enqueue(object : Callback<List<Joke>> {
            override fun onResponse(call: Call<List<Joke>>?, response: Response<List<Joke>>?) {
                response?.body()?.let {
                    callback.callback(it)
                }
            }
            override fun onFailure(call: Call<List<Joke>>?, t: Throwable?) {

            }
        })
    }

    interface JokesService {
        @GET("jokes/random")
        fun joke(): Call<Joke>

        @GET("jokes/{id}")
        fun joke(@Path("id") id: String): Call<Joke>

        @GET("jokes/random")
        fun fromCategory(@Query("category") category: String): Call<Joke>

        @GET("jokes/search")
        fun search(@Query("query") query: String): Call<List<Joke>>
    }
}