package com.fungisoft.testingchuck.network

interface CategoriesProvider {
    fun categories(callback: CategoriesListener)
}