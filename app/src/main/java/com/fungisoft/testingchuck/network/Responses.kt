package com.fungisoft.testingchuck.network

import com.fungisoft.testingchuck.models.Joke

interface JokeListener {
    fun callback(joke: Joke)
}

interface JokesListener {
    fun callback(jokes: List<Joke>)
}

interface CategoriesListener {
    fun categoriesCallback(categories: List<String>)
}