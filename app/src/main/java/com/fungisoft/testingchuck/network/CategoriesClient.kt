package com.fungisoft.testingchuck.network

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.GET

class CategoriesClient : BaseClient(), CategoriesProvider {
    private val service = retrofit.create(CategoriesService::class.java)

    override fun categories(callback: CategoriesListener) {
        service.categories().enqueue(object: Callback<List<String>> {
            override fun onResponse(call: Call<List<String>>?, response: Response<List<String>>?) {
                response?.body()?.let {
                    callback.categoriesCallback(it)
                }
            }

            override fun onFailure(call: Call<List<String>>?, t: Throwable?) {

            }
        })
    }

    interface CategoriesService {
        @GET("jokes/categories")
        fun categories(): Call<List<String>>
    }
}
