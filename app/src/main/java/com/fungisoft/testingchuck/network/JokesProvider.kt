package com.fungisoft.testingchuck.network

interface JokesProvider {
    fun joke(callback: JokeListener)
    fun joke(id: String, callback: JokeListener)
    fun fromCategory(category: String, callback: JokeListener)
    fun search(text: String, callback: JokesListener)
}
