package com.fungisoft.testingchuck.presentation

import com.fungisoft.testingchuck.models.Joke
import com.fungisoft.testingchuck.network.*

class MainPresenterImp(private var mainView: MainView) : MainPresenter, JokeListener, JokesListener, CategoriesListener {
    var jokesProvider: JokesProvider = JokesClient()
    var categoriesProvider: CategoriesProvider = CategoriesClient()

    override fun jokeForCategory(category: String) {
        jokesProvider.fromCategory(category, this)
    }

    override fun search(text: String) {
        jokesProvider.search(text, this)
    }

    override fun categories() {
        categoriesProvider.categories(this)
    }

    override fun randomJoke() {
        if (superComplicatedMethod()) {
            jokesProvider.joke(this)
        }
    }

    fun superComplicatedMethod(): Boolean {
        // This method is super complicated
        return true
    }

    override fun callback(joke: Joke) {
        mainView.showJoke(joke)
    }

    override fun callback(jokes: List<Joke>) {
        mainView.showJokes(jokes)
    }

    override fun categoriesCallback(categories: List<String>) {
        mainView.showCategories(categories)
    }
}