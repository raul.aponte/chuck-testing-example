package com.fungisoft.testingchuck.presentation

import com.fungisoft.testingchuck.models.Joke

interface MainPresenter {
    fun jokeForCategory(category: String)
    fun search(text: String)
    fun categories()
    fun randomJoke()
}

interface MainView {
    fun showJoke(joke: Joke)
    fun showJokes(jokes: List<Joke>)
    fun showCategories(categories: List<String>)
}