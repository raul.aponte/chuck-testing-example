package com.fungisoft.testingchuck

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.fungisoft.testingchuck.models.Joke
import com.fungisoft.testingchuck.network.*
import com.fungisoft.testingchuck.presentation.MainPresenter
import com.fungisoft.testingchuck.presentation.MainPresenterImp
import com.fungisoft.testingchuck.presentation.MainView
import com.fungisoft.testingchuck.views.CategoriesFragment
import com.fungisoft.testingchuck.views.DashboardFragment
import com.fungisoft.testingchuck.views.JokeContentFragment
import com.fungisoft.testingchuck.views.navigation.NavigationListener

class MainActivity : AppCompatActivity(), NavigationListener, MainView {
    var mainPresenter: MainPresenter? = null
    private var jokesListener: JokesListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainPresenter = MainPresenterImp(this)

        val fragment = DashboardFragment.newInstance(this)
        fragmentManager.beginTransaction()
                .replace(R.id.layoutMainContent, fragment)
                .commit()
    }

    override fun onBackPressed() {
        if (fragmentManager.backStackEntryCount > 0) {
            fragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    override fun onCategorySelected(category: String) {
        mainPresenter?.jokeForCategory(category)
    }

    override fun searchForJoke(text: String, callback: JokesListener) {
        jokesListener = callback
        mainPresenter?.search(text)
    }

    override fun showCategories() {
        mainPresenter?.categories()
    }

    override fun showRandomJoke() {
        mainPresenter?.randomJoke()
    }

    override fun showJoke(joke: Joke) {
        fragmentManager.beginTransaction()
                .replace(R.id.layoutMainContent, JokeContentFragment.newInstance(joke))
                .addToBackStack("")
                .commit()
    }

    override fun showJokes(jokes: List<Joke>) {
        jokesListener?.callback(jokes)
    }

    override fun showCategories(categories: List<String>) {
        fragmentManager.beginTransaction()
                .replace(R.id.layoutMainContent, CategoriesFragment.newInstance(categories, this@MainActivity))
                .addToBackStack("")
                .commit()
    }
}
