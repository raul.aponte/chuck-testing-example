package com.fungisoft.testingchuck

import com.fungisoft.testingchuck.network.CategoriesProvider
import com.fungisoft.testingchuck.network.JokesProvider
import com.fungisoft.testingchuck.presentation.MainPresenterImp
import com.fungisoft.testingchuck.presentation.MainView
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations.initMocks

import org.junit.Assert.*
import org.mockito.Mockito
import org.mockito.Mockito.*

class MainPresenterTest {
    @Mock
    private val mainView = mock(MainView::class.java)

    @Mock
    private val jokesProvider = mock(JokesProvider::class.java)

    @Mock
    private val categoriesProvider = mock(CategoriesProvider::class.java)

    @JvmField
    @InjectMocks
    var mainPresenter: MainPresenterImp? = null

    @Before
    fun setUp() {
        mainPresenter = MainPresenterImp(mainView)
        initMocks(this)
    }

    @Test
    fun test_super_complicated_method() {
        assertEquals("Super Complicated Method returns true",true, mainPresenter?.superComplicatedMethod())
    }

    @Test
    fun test_random_joke_call() {
        mainPresenter?.randomJoke()
        verify(jokesProvider).joke(any())
    }

    private fun <T> any(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    private fun <T> uninitialized(): T = null as T
}