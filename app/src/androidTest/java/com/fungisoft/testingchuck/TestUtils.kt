package com.fungisoft.testingchuck

import com.fungisoft.testingchuck.models.Joke

fun dummyJoke(): Joke {
    return Joke(
            "0",
            "Content",
            "Funny",
            null
    )
}

fun dummyCategories(): List<String> {
    return listOf("Blah", "Meh")
}