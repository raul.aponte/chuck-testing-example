package com.fungisoft.testingchuck

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.Espresso.pressBack
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.fungisoft.testingchuck.presentation.MainPresenter
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations.initMocks

@RunWith(AndroidJUnit4::class)
class DashboardFragmentTest {
    @Rule
    @JvmField
    val mainRule = ActivityTestRule<MainActivity>(MainActivity::class.java)

    @Mock
    private val mainPresenter = mock(MainPresenter::class.java)

    @JvmField
    @InjectMocks
    var activity: MainActivity? = null

    @Before
    fun setUp() {
        activity = mainRule.activity
        activity?.fragmentManager?.beginTransaction()
        initMocks(this)
    }

    @After
    fun tearDown() {
        pressBack()
    }

    @Test
    fun categories_navigation_test() {
        onView(withId(R.id.categoriesButton))
                .perform(click())
        verify(mainPresenter).categories()
        activity?.showCategories(dummyCategories())
        onView(withText(R.string.categories))
                .check(matches(isDisplayed()))
    }

    @Test
    fun jokes_navigation_test() {
        onView(withId(R.id.randomButton))
                .perform(click())
        verify(mainPresenter).randomJoke()
        activity?.showJoke(dummyJoke())
        onView(withText(R.string.joke))
                .check(matches(isDisplayed()))
    }
}